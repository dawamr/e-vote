<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>OPUSNUSANTARA {{Date('Y')}}</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="/stisla/assets/modules/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="/stisla/assets/modules/fontawesome/css/all.min.css">
  
  <!-- CSS Libraries -->
  
  <!-- <link rel="stylesheet" href="/stisla/assets/modules/prism/prism.css"> -->
  <!-- Template CSS -->
  <link rel="stylesheet" href="/stisla/assets/css/style.css">
  <link rel="stylesheet" href="/stisla/assets/css/components.css">

  <script src="/stisla/assets/modules/jquery.min.js"></script>

<style>
  .no-gutter > [class*='col-'] {
      padding-right:0 !important;
      padding-left:0 !important;
  }
  .card .card-header{
    border-bottom: 1px solid rgba(0, 0, 0, 0.125) !important;
  }
  .card .card-footer{
    border-top: 1px solid rgba(0, 0, 0, 0.125) !important;
  }
  .card .card-body p {
    font-size:15px;
  }
  body{
    color:#43474a;
  }
  .h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6{
    color: rgb(64, 64, 66);
  }
  .btn-inline{
    border-radius: 0px;
    padding-bottom: 10px;
  }
  .select-inline{
    border-radius: 0px
  }
  @media (max-width: 700px){
    #times{
      visibility: hidden;
    }
  }
</style>
<link rel="stylesheet" href="/ubold/assets/css/icons.css">
@yield('css')
<!-- /END GA -->
</head>

<body id="body">
  <div id="app">
    <div class="main-wrapper main-wrapper-1">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar">
        <form class="form-inline mr-auto">
          <ul class="navbar-nav mr-3" width="100%">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
            <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="fas fa-search"></i></a></li>
          </ul>
        </form>
        <ul>
        
        </ul>
        <ul class="navbar-nav navbar-right" style="display:block;width:100%">
        <li>
        <div class="row">
          <div class="col-12 col-md-9"><marquee direction="left" class="text-white">
                PEMILOS TAHUN INI
            </marquee></div>
          <div class="col-3" id="times">
            <div class=" text-white">{{\Carbon\Carbon::Now()->format('l, d F Y')}}</div> 
          </div>
        </div>
         
        </li>
        </ul>
      </nav>
      <div class="main-sidebar sidebar-style-2">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand" id="SWellcome">
            <a href="#">PEMILOS+</a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">{{Date('Y')}}</a>
          </div>
          <ul class="sidebar-menu">
            <li class="menu-header">DASHBOARD</li>
            <li><a class="nav-link" href="/v2/"><i class="fas fa-home" aria-hidden="true"></i> <span>Beranda</span></a></li>
            <li class="menu-header">SETTING</li>
            <!-- <li><a class="nav-link" href="/v2/streaming"><i class="far fa-play-circle" aria-hidden="true"></i> <span>Streaming</span></a></li> -->
            <li><a class="nav-link" href="/v2/">&nbsp;&nbsp; <i class="md md-assignment-ind" aria-hidden="true"></i> <span>Pengaturan Pemilos</span></a></li>
            <li><a class="nav-link" href="/v2/"><i class="far fa-play-circle" aria-hidden="true"></i> <span>Pasangan Calon</span></a></li>
            <li><a class="nav-link" href="/v2/"><i class="far fa-play-circle" aria-hidden="true"></i> <span>Data Pemilih</span></a></li>
            <li><a class="nav-link" href="/v2/"><i class="far fa-play-circle" aria-hidden="true"></i> <span>Hasil Pemilos</span></a></li>
            <li><a class="nav-link" href="/v2/"><i class="far fa-play-circle" aria-hidden="true"></i> <span>Pendeteksi Anomali</span></a></li>
            <li class="menu-header">Report</li>
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown"><i class="far fa-newspaper"></i><span>Laporan</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="/v2/">Semua Data</a></li>
                <li><a class="nav-link" href="/v2/">Hasil Pemilos</a></li>
              </ul>
            </li>
          </ul>

          <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
            <a href="/v2/login" class="btn btn-success btn-lg btn-block btn-icon-split">
              {{strtoupper('ACTIVED')}}
            </a>
          </div>
      </div>
      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
          <div class="section-body">
            @yield('content')
            
          </div>
        </section>
      </div>
      <footer class="main-footer">
        <div class="footer-left">
          Copyright &copy; Dawam Raja</a>
        </div>
        <div class="footer-right">

        </div>
      </footer>
    </div>
  </div>
  <!-- General JS Scripts -->
  <script src="/stisla/assets/modules/popper.js"></script>
  <script src="/stisla/assets/modules/tooltip.js"></script>
  <script src="/stisla/assets/modules/bootstrap/js/bootstrap.min.js"></script>
  <script src="/stisla/assets/modules/nicescroll/jquery.nicescroll.min.js"></script>
  <script src="/stisla/assets/modules/moment.min.js"></script>
  <script src="/stisla/assets/js/stisla.js"></script>
  
  <!-- JS Libraies -->
  <!-- <script src="/stisla/assets/modules/prism/prism.js"></script> -->
@yield('js')
  <!-- Page Specific JS File -->

  <!-- Template JS File -->
  <script src="/stisla/assets/js/scripts.js"></script>
  <script src="/stisla/assets/js/custom.js"></script>
</body>
</html>
