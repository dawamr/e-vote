@extends('layouts.admin')

@section('css')

@endsection

@section('content')
<div class="section-header">
    <h1>Beranda</h1>
</div>
<div class="row">
    <!-- GRAFIK -->
    <div class="col-12 col-sm-12 col-lg-12">
        <i>Tanggal Pemilihan : Kamis, 11 OKTOBER 2019 | 08:00 - 12:00</i>
    </div>
    <div class="col-12 col-sm-12 col-lg-12 ">
        <div class="card author-box card-primary">
            <div class="card-header">
                <b>R E A L &nbsp COUNT</b>
            </div>
            <div class="card-body">
            </div>
        </div>
    </div>
    <!-- Presentase -->
    <div class="col-12 col-sm-12 col-lg-12 ">
        <div class="card author-box card-primary">
            <div class="card-header">
                <b>PRESENTASE</b>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6 card">
                        <div class="card-header card-secondary text-center">SUDAH MEMILIH <hr><b>90%</b></div>
                        <div class="card-body text-center m-2">
                            <h2>180</h2>Peserta Pemilos
                        </div>
                    </div>
                    <div class="col-md-6 card">
                        <div class="card-header card-secondary text-center">BELUM MEMILIH <hr><b>10%</b></div>
                        <div class="card-body text-center m-2">
                            <h2>20</h2>Peserta Pemilos
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    // $(document).ready(function(){
    //     window.open("https://translate.google.com/translate_tts?ie=UTF-8&client=tw-ob&tl=en&q={{str_replace(' ','+','Hi , Wellcome to Opusnusantara , Keep Smile Today')}}+");
    // })
  </script>
@endsection

