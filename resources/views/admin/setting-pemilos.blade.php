@extends('layouts.admin')

@section('css')
<link rel="stylesheet" href="/stisla/assets/modules/bootstrap-daterangepicker/daterangepicker.css">
<link rel="stylesheet" href="/stisla/assets/modules/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
@endsection

@section('content')
<div class="section-header">
    <h1>Pengaturan Pemilos</h1>
</div>
<div class="row">
    <div class="col-12 col-sm-12 col-lg-12 ">
        <div class="card card-primary">
            <div class="card-body">
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label>Judul Pemilos</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Abc</div>  
                            </div>
                            <input type="text" class="form-control" name="judul" id="judul" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Abc</div>  
                            </div>
                            <input type="text" class="form-control" name="keterangan" id="keterangan" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Waktu Pelaksanaan<code>*</code></label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fas fa-calendar"></i>
                            </div>  
                            </div>
                            <input type="text" class="form-control datepicker" id="tangal" name="tangal" required>
                        </div>             
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Mulai Pukul</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fas fa-clock"></i>
                                    </div>
                                    </div>
                                    <input type="text" class="form-control timepicker">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Selesai Pukul</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fas fa-clock"></i>
                                    </div>
                                    </div>
                                    <input type="text" class="form-control timepicker">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Jumlah Peserta Pemilos</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">123</div>  
                            </div>
                            <input type="number" min="10" max="2000" class="form-control" name="total" id="total" required>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 text-center">
                    <div class="form-group">
                        <label>Upload Logo</label>
                        <input type="file" name="logo" id="logo" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Upload Banner</label>
                        <input type="file" name="banner" id="banner" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Preview Logo</label>
                        <div id="preview"></div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label>Preview Banner</label>
                        <div id="preview2"></div>
                    </div>
                    <button type="submit" class="btn btn-lg btn-block btn-primary"> Simpan Pengaturan</button>
                </div>
            </div>
            </div>
        </div>
    </div>
    
</div>
@endsection

@section('js')
<script src="/stisla/assets/modules/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="/stisla/assets/modules/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script>
    // $(document).ready(function(){
    //     window.open("https://translate.google.com/translate_tts?ie=UTF-8&client=tw-ob&tl=en&q={{str_replace(' ','+','Hi , Wellcome to Opusnusantara , Keep Smile Today')}}+");
    // })
  </script>
@endsection

