@extends('layouts.admin')

@section('css')
<link rel="stylesheet" href="/stisla/assets/modules/bootstrap-daterangepicker/daterangepicker.css">
<link rel="stylesheet" href="/stisla/assets/modules/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
<style>
.flex-container {
  display: flex;
  justify-content: center;
  align-items: center;
  height: 300px;
}


</style>
@endsection

@section('content')
<div class="section-header">
    <h1>Pengaturan Pemilos</h1>
</div>
<!-- <div class="flex-container">
    <div class="col-4 text-center" style="border: 1px black">
        <a href=""><i style="font-size: 4em; justify-content; flex-center; align-item: center" class="fas fa-user-plus"></i></a><br>
        <b>Belum Ada Calon</b>
    </div>
</div> -->
<div class="row">
    <div class="col-lg-12col-md-6 col-sm-12">
        <div class="card card-primary">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-9"><b>Pasangan Calon #1</b></div><div class="col-md-3"><a href="http://" target="_blank" rel="noopener noreferrer">Tampilkan lebih</a></div>
                    <div class="col-6 col-sm-4 mt-3">Dawam Raja</div>
                    <div class="col-6 col-sm-3 mt-3">Calon Ketua</div>
                    <div class="col-12 col-sm-5 text-center mt-3">Foto</div>           
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="/stisla/assets/modules/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="/stisla/assets/modules/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script>
    // $(document).ready(function(){
    //     window.open("https://translate.google.com/translate_tts?ie=UTF-8&client=tw-ob&tl=en&q={{str_replace(' ','+','Hi , Wellcome to Opusnusantara , Keep Smile Today')}}+");
    // })
  </script>
@endsection

